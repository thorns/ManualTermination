#include <stdlib.h>
#include <string.h>

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Termination.h"
#include "cctk_Timers.h"

static CCTK_REAL watchminutes;

void ManualTermination_StartTimer (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int retval = 0;
  int ierr;
  int TimerIndex;

  TimerIndex = CCTK_TimerCreate ("WatchWalltime");

  ierr = CCTK_TimerStart ("WatchWalltime");

  watchminutes = output_remtime_every_minutes*1.0e0;

  CCTK_VInfo (CCTK_THORNSTRING, "Started Timer");
  CCTK_VInfo (CCTK_THORNSTRING, "Reminding you every %d "
                                "minutes of remaining walltime.",
                                output_remtime_every_minutes);
  return;
}

void ManualTermination_ResetMinutes (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  watchminutes = output_remtime_every_minutes*1.0e0;

  return;
}

void ManualTermination_CheckWalltime (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int retval = 0,ierr;
  cTimerData *info;
  const cTimerVal *walltime;
  CCTK_REAL time;

  if (CCTK_MyProc (cctkGH) != 0) /* only root process queries elapsed runtime */
  {
    return;
  }
  if (on_remaining_walltime <= 0)
    return;

  info = CCTK_TimerCreateData ();
  ierr = CCTK_Timer ("WatchWalltime",info);

  ierr = CCTK_TimerStop ("WatchWalltime");
  /* get walltime */
  walltime = CCTK_GetClockValue ("gettimeofday",info);
  time = CCTK_TimerClockSeconds (walltime);
  CCTK_TimerDestroyData (info);

  ierr = CCTK_TimerStart ("WatchWalltime");

  if ((time/60.0e0 > watchminutes) && watchminutes != 0)
  {
    watchminutes = (watchminutes)+output_remtime_every_minutes*1.0e0;
    CCTK_INFO ("------------------------------------------------------");
    CCTK_VInfo (CCTK_THORNSTRING, "Remaining walltime is %1.2f minutes. :-)",
		    (max_walltime*60.0-time/60.0));
    CCTK_INFO ("------------------------------------------------------");
  } 
  
  if (time/60.0e0 >= (max_walltime*60.0e0 - on_remaining_walltime*1.0e0))
  {
    CCTK_INFO ("------------------------------------------------------");
    CCTK_VInfo (CCTK_THORNSTRING, "Remaining walltime is %1.2f minutes. \n"
              "Triggering termination ...", (max_walltime*60.0-time/60.0));
    CCTK_INFO ("------------------------------------------------------");
    CCTK_TerminateNext (cctkGH);
  }

  return;
}
