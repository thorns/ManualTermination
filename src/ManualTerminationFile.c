#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Termination.h"
/* The termination file only lives on the root node, so the scheduled
 * functions here always check that they are on that node.
 */
enum{ BUFLEN = 128 };
/* On first call, pass parameter terminate_filename.  
   If it is null will construct file name in /tmp based on PBS_JOBID.
   Subsequent calls ignore the argument, and return a static buffer.
*/
const char * MT_get_terminate_filename (CCTK_STRING terminate_filename)
{
  static char buf[BUFLEN];

  if (strlen (buf) != 0)
    return buf;

  if (strlen (terminate_filename) == 0)
  {
    const char * pbs_jobid = getenv ("MANUAL_TERMINATION_JOB_ID");
    if (pbs_jobid == NULL)
       pbs_jobid = getenv ("PBS_JOBID");

    if (pbs_jobid == NULL)
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, "ManualTermination",
                   "Could not find environment variable "
                   "'MANUAL_TERMINATION_JOB_ID' or 'PBS_JOBID'"
                 );
    else
      snprintf (buf, BUFLEN, "/tmp/cactus_terminate.%s", pbs_jobid);
  }
  else
  {
    snprintf (buf, BUFLEN, "%s", terminate_filename);
  }
  return buf;
}

void ManualTermination_Init (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (termination_from_file && CCTK_MyProc (cctkGH) == 0)
  {
    FILE *termfile = fopen (MT_get_terminate_filename (termination_file), "w");
    if (termfile != NULL)
    {
      fprintf (termfile, "%d", 0);
      fclose (termfile);
      chmod(MT_get_terminate_filename (NULL),
                                     S_IRUSR|S_IRGRP|S_IWUSR|S_IWGRP);
    }
    else
    {
      CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, "ManualTermination",
                   "Could not open termination file '%s'. Error: %s",
                   MT_get_terminate_filename (NULL), strerror (errno));
    }
  }
  return;
}

void ManualTerminationFile (CCTK_ARGUMENTS)
{
  int terminate;
  FILE *terminationfile;

  DECLARE_CCTK_PARAMETERS;

  /* only one processor needs to query the elapsed runtime */
  if (CCTK_MyProc (cctkGH) != 0)
  {
    return;
  }

  if (((cctkGH->cctk_iteration- 1) % check_file_every * 1.0e0) != 0) 
  {
    return;
  }

  terminationfile = fopen (MT_get_terminate_filename (NULL), "r");

  if (terminationfile != NULL)
  {
    terminate = 0;
    fscanf (terminationfile, "%d", &terminate);
    fclose (terminationfile);
    
    if (terminate == 1)
    {
      CCTK_INFO ("------------------------------------------------------");
      CCTK_VInfo (CCTK_THORNSTRING, "OH MY GOD! Found termination signal "
                                    "in termination file! TERMINATION NOW!!!!");
      CCTK_INFO ("------------------------------------------------------");
      CCTK_TerminateNext (cctkGH);
    }
  }

  return;
}

//int ManualTermination_Cleanup (CCTK_ARGUMENTS)
int ManualTermination_Cleanup (void)
{
  DECLARE_CCTK_PARAMETERS;

  if (termination_from_file
  && CCTK_MyProc (NULL) == 0
  && MT_get_terminate_filename (NULL))
    remove (MT_get_terminate_filename (NULL));
  return 0;
}
